package com.ggs.magicpencil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ggs.magicpencil.act.HowToDraw;
import com.ggs.magicpencil.act.WhatCollors;
import com.ggs.magicpencil.act.WhatTime;
import com.ggs.magicpencil.act.painter;

public class MainActivity extends AppCompatActivity {
View rectangle_1, rectangle_2, rectangle_3, rectangle_4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rectangle_1=findViewById(R.id.rectangle_1);
        rectangle_2=findViewById(R.id.rectangle_2);
        rectangle_3=findViewById(R.id.rectangle_3);
        rectangle_4=findViewById(R.id.rectangle_4);



        rectangle_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startP=new Intent(MainActivity.this, painter.class);
                startActivity(startP);
            }
        });

        rectangle_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent start2=new Intent(MainActivity.this, HowToDraw.class);
                startActivity(start2);
            }
        });

        rectangle_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent start3=new Intent(MainActivity.this, WhatCollors.class);
                startActivity(start3);
            }
        });

        rectangle_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent start4=new Intent(MainActivity.this, WhatTime.class);
                startActivity(start4);
            }
        });


    }
}